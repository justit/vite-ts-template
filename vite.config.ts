import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslint from 'vite-plugin-eslint'
import viteCompression from 'vite-plugin-compression'
import path from 'path'
import Pages from 'vite-plugin-pages'
import Components from 'unplugin-vue-components/vite'
import AutoImport from 'unplugin-auto-import/vite'
import UnoCSS from 'unocss/vite'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  },
  plugins: [
    vue(),
    eslint(),
    viteCompression({
      threshold: 10240
    }),
    Pages({
      extensions: ['vue'],
      exclude: ['**/components/*.vue']
    }),
    Components({
      globs: ['src/components/*/*.vue'],
      dts: './src/types/components.d.ts'
    }),
    AutoImport({
      eslintrc: {
        enabled: false, // Default `false`
        filepath: './src/types/.eslintrc-auto-import.json', // Default `./.eslintrc-auto-import.json`
        globalsPropValue: true // Default `true`, (true | false | 'readonly' | 'readable' | 'writable' | 'writeable')
      },
      imports: [
        // 插件预设支持导入的api
        'vue',
        'vue-router',
        'pinia'
        // 自定义导入的api
      ],
      dts: './src/types/auto-imports.d.ts'
    }),
    UnoCSS({
      configFile: '../uno.config.ts'
    })
  ],
  server: {
    host: '0.0.0.0'
  },
  build: {
    minify: 'terser',
    terserOptions: {
      compress: {
        drop_console: true,
        drop_debugger: true
      }
    }
  }
})
