import { createApp } from 'vue'
import App from './App.vue'
import plugin from '@/plugins/index.js'
import 'uno.css'

const app = createApp(App)

app.use(plugin)
app.mount('#app')
