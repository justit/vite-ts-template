import { App } from 'vue'
const modulesFiles = import.meta.glob('./modules/*.ts', { eager: true })

export default {
  install (app: App) {
    Object.values(modulesFiles).forEach((module:any) => {
      module.default(app)
    })
  }
}
